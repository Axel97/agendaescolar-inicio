﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgendaEscolar
{
    public partial class vTareas : Form
    {
        Form1 principal;

        public vTareas(Form1 f)
        {
            InitializeComponent();
            principal = f;
        }

        private void vTareas_FormClosing(object sender, FormClosingEventArgs e)
        {
            principal.abierto = false;
        }
    }
}
