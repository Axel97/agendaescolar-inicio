﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgendaEscolar
{
    public partial class eTareas : Form
    {

        Form1 principal;
        public eTareas(Form1 f)
        {
            InitializeComponent();
            principal = f;
        }

        private void eTareas_FormClosing(object sender, FormClosingEventArgs e)
        {
            principal.abierto = false;        }
    }
}
