﻿namespace AgendaEscolar
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.oPCIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aGREGARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tAREASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mODIFICARTAREASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eLIMINARTAREASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oPCIONESToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // oPCIONESToolStripMenuItem
            // 
            this.oPCIONESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aGREGARToolStripMenuItem,
            this.tAREASToolStripMenuItem,
            this.mODIFICARTAREASToolStripMenuItem,
            this.eLIMINARTAREASToolStripMenuItem});
            this.oPCIONESToolStripMenuItem.Name = "oPCIONESToolStripMenuItem";
            this.oPCIONESToolStripMenuItem.Size = new System.Drawing.Size(106, 20);
            this.oPCIONESToolStripMenuItem.Text = " MENU AGENDA";
            // 
            // aGREGARToolStripMenuItem
            // 
            this.aGREGARToolStripMenuItem.Name = "aGREGARToolStripMenuItem";
            this.aGREGARToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aGREGARToolStripMenuItem.Text = "AGREGAR TAREAS";
            this.aGREGARToolStripMenuItem.Click += new System.EventHandler(this.aGREGARToolStripMenuItem_Click);
            // 
            // tAREASToolStripMenuItem
            // 
            this.tAREASToolStripMenuItem.Name = "tAREASToolStripMenuItem";
            this.tAREASToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tAREASToolStripMenuItem.Text = "VER TAREAS";
            this.tAREASToolStripMenuItem.Click += new System.EventHandler(this.tAREASToolStripMenuItem_Click);
            // 
            // mODIFICARTAREASToolStripMenuItem
            // 
            this.mODIFICARTAREASToolStripMenuItem.Name = "mODIFICARTAREASToolStripMenuItem";
            this.mODIFICARTAREASToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.mODIFICARTAREASToolStripMenuItem.Text = "MODIFICAR TAREAS";
            this.mODIFICARTAREASToolStripMenuItem.Click += new System.EventHandler(this.mODIFICARTAREASToolStripMenuItem_Click);
            // 
            // eLIMINARTAREASToolStripMenuItem
            // 
            this.eLIMINARTAREASToolStripMenuItem.Name = "eLIMINARTAREASToolStripMenuItem";
            this.eLIMINARTAREASToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.eLIMINARTAREASToolStripMenuItem.Text = "ELIMINAR TAREAS";
            this.eLIMINARTAREASToolStripMenuItem.Click += new System.EventHandler(this.eLIMINARTAREASToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Agenda Escolar";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem oPCIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aGREGARToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tAREASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mODIFICARTAREASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eLIMINARTAREASToolStripMenuItem;
    }
}

