﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgendaEscolar
{
    public partial class Form1 : Form
    {

        public bool abierto = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void tAREASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!abierto)
            {
                vTareas mVentana = new vTareas(this);
                mVentana.MdiParent = this;
                mVentana.Show();
                mVentana.WindowState = FormWindowState.Maximized;
                abierto = false;
            }
        }

        private void aGREGARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!abierto)
            {
                aTareas mVentana = new aTareas(this);
                mVentana.MdiParent = this;
                mVentana.Show();
                mVentana.WindowState = FormWindowState.Maximized;
                abierto = false;
            }
        }

        private void mODIFICARTAREASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!abierto)
            {
                mTareas mVentana = new mTareas(this);
                mVentana.MdiParent = this;
                mVentana.Show();
                mVentana.WindowState = FormWindowState.Maximized;
                abierto = false;
            }
        }

        private void eLIMINARTAREASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!abierto)
            {
                eTareas mVentana = new eTareas(this);
                mVentana.MdiParent = this;
                mVentana.Show();
                mVentana.WindowState = FormWindowState.Maximized;
                abierto = false;
            }
        }
    }
}
